package com.mbkopmaz.truecallergrabber.task;

import android.content.Context;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class TaskHandlerTest {

    @Mock
    private Context context;
    @Mock
    Task<Character> task1;
    @Mock
    Task<Character[]> task2;
    @Mock
    Task<Integer> task3;
    @Mock
    TaskHandler.UiListener uiListener;
    private TaskHandler taskHandler;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
        taskHandler = new TaskHandler(context, task1, task2, task3);
        taskHandler.setUiListener(uiListener);
    }

    @Test
    public void checkIfTasksFinished() throws Exception {
        when(task1.isFinished()).thenReturn(true);
        when(task2.isFinished()).thenReturn(true);
        when(task3.isFinished()).thenReturn(true);

        boolean result = taskHandler.areTasksFinished();
        assertTrue(result);
    }

    @Test
    public void shouldCallListenerWhenAllTasksFinished() throws Exception {
        when(task1.isFinished()).thenReturn(true);
        when(task2.isFinished()).thenReturn(true);
        when(task3.isFinished()).thenReturn(true);

        taskHandler.checkIfTasksFinished();

        verify(uiListener).onTasksFinished();
    }

    @Test
    public void shouldNeverCallListenerWhenAtLeastOneTaskNotFinished() throws Exception {
        when(task1.isFinished()).thenReturn(true);
        when(task2.isFinished()).thenReturn(true);
        when(task3.isFinished()).thenReturn(false);

        taskHandler.checkIfTasksFinished();

        verify(uiListener, never()).onTasksFinished();
    }
}

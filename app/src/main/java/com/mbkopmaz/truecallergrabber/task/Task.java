package com.mbkopmaz.truecallergrabber.task;

import com.mbkopmaz.truecallergrabber.util.Constants;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public abstract class Task<T> {

    private final OkHttpClient client;

    private Listener<T> listener;

    protected int errorCode = 0;
    protected boolean isFinished = false;

    Task(OkHttpClient client) {
        this.client = client;
    }

    void setListener(Listener<T> listener) {
        this.listener = listener;
    }

    public void run() {
        reset();

        Request request = new Request.Builder()
                .url(Constants.URL)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                listener.onFailure();
                isFinished = true;
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    if (listener != null) {
                        listener.onFailure();
                        response.close();
                    }
                    isFinished = true;
                    throw new IOException("Unexpected Code: " + response);
                }

                String pageContent = response.body().string();
                T result = processContent(pageContent);
                if (listener != null) {
                    listener.onUpdate(result);
                }
                isFinished = true;
            }
        });
    }

    private void reset() {
        errorCode = 0;
        isFinished = false;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public int getErrorCode() {
        return errorCode;
    }

    protected abstract T processContent(String content);

    interface Listener<T> {
        void onUpdate(T update);

        void onFailure();
    }
}

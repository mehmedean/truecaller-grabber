package com.mbkopmaz.truecallergrabber.task;

import android.content.Context;
import android.os.Handler;

import com.mbkopmaz.truecallergrabber.R;
import com.mbkopmaz.truecallergrabber.util.Constants;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;

public class TaskHandler {

    private final Task<Character> theIndexthCharacterTask;
    private final Task<Character[]> everyIndexthCharacterTask;
    private final Task<Integer> wordCounterTask;
    private List<Task> tasks;
    private Context context;
    private Handler mainHandler;
    private UiListener uiListener;
    private boolean isFailureProcessed = false;

    public TaskHandler(Context context) {
        this(context, new OkHttpClient());
    }

    TaskHandler(Context context, OkHttpClient client) {
        this(context, new TheIndexthCharacterTask(client), new EveryIndexthCharacterTask(client), new WordCounterTask(client));
    }

    TaskHandler(Context context,
                Task<Character> theIndexthCharacterTask,
                Task<Character[]> everyIndexthCharacterTask,
                Task<Integer> wordCounterTask) {
        this.context = context;
        this.theIndexthCharacterTask = theIndexthCharacterTask;
        this.everyIndexthCharacterTask = everyIndexthCharacterTask;
        this.wordCounterTask = wordCounterTask;
        this.mainHandler = new Handler(context.getMainLooper());
        initializeTasks();
        reset();
    }

    public void setUiListener(UiListener uiListener) {
        this.uiListener = uiListener;
    }

    private void reset() {
        isFailureProcessed = false;
    }

    public void initializeTasks() {
        theIndexthCharacterTask.setListener(new Task.Listener<Character>() {
            @Override
            public void onUpdate(final Character character) {
                mainHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        String text;
                        if (character != null) {
                            text = context.getString(R.string.indexth_char_text,
                                    Constants.INDEX, (char) character, (int) character);
                        } else {
                            switch (theIndexthCharacterTask.getErrorCode()) {
                                case Constants.ERROR_CODE_CONTENT_TOO_SHORT:
                                    text = context.getString(R.string.error_content_too_short, Constants.INDEX);
                                    break;
                                default:
                                    text = context.getString(R.string.error_processing_unsuccessful);
                                    break;
                            }
                        }
                        uiListener.onTask1Finished(text);
                        checkIfTasksFinished();
                    }
                });
            }

            @Override
            public void onFailure() {
                processFailure();
            }
        });

        everyIndexthCharacterTask.setListener(new Task.Listener<Character[]>() {
            @Override
            public void onUpdate(final Character[] characters) {
                mainHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (characters != null) {
                            for (int i = 0; i < characters.length; i++) {
                                uiListener.appendTask2Text(context.getString(R.string.every_indexth_char_text,
                                        ((i + 1) * Constants.INDEX), (char) characters[i], (int) characters[i]));
                            }
                        } else {
                            switch (everyIndexthCharacterTask.getErrorCode()) {
                                case Constants.ERROR_CODE_CONTENT_TOO_SHORT:
                                    uiListener.appendTask2Text(context.getString(R.string.error_content_too_short, Constants.INDEX));
                                    break;
                                default:
                                    uiListener.appendTask2Text(context.getString(R.string.error_processing_unsuccessful));
                                    break;
                            }
                        }
                        uiListener.onTask2Finished();
                        checkIfTasksFinished();
                    }
                });
            }

            @Override
            public void onFailure() {
                processFailure();
            }
        });

        wordCounterTask.setListener(new Task.Listener<Integer>() {
            @Override
            public void onUpdate(final Integer count) {
                mainHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        String text = "";
                        if (count != null) {
                            text = context.getString(R.string.word_counter_text, Constants.KEY, count);
                        } else {
                            switch (wordCounterTask.getErrorCode()) {
                                default:
                                    text = context.getString(R.string.error_processing_unsuccessful);
                                    break;
                            }
                        }
                        uiListener.onTask3Finished(text);
                        checkIfTasksFinished();
                    }
                });
            }

            @Override
            public void onFailure() {
                processFailure();
            }
        });

        tasks = new ArrayList<>();
        tasks.add(theIndexthCharacterTask);
        tasks.add(everyIndexthCharacterTask);
        tasks.add(wordCounterTask);
    }

    public void runTasks() {
        reset();
        for (Task task : tasks) {
            task.run();
        }
    }

    public boolean areTasksFinished() {
        boolean areTasksFinished = true;
        for (Task task : tasks) {
            if (!task.isFinished()) {
                areTasksFinished = false;
                break;
            }
        }
        return areTasksFinished;
    }

    void checkIfTasksFinished() {
        if (areTasksFinished()) {
            uiListener.onTasksFinished();
        }
    }

    private void processFailure() {
        if (!isFailureProcessed) {
            mainHandler.post(new Runnable() {
                @Override
                public void run() {
                    uiListener.showAlertDialog(context.getString(R.string.generic_error));
                }
            });
            isFailureProcessed = true;
        }
    }

    public interface UiListener {
        void appendTask2Text(String text);

        void onTask1Finished(String text);

        void onTask2Finished();

        void onTask3Finished(String text);

        void onTasksFinished();

        void showAlertDialog(String text);
    }
}

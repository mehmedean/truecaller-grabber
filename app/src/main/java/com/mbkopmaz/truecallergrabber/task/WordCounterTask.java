package com.mbkopmaz.truecallergrabber.task;

import com.mbkopmaz.truecallergrabber.util.Constants;

import java.util.HashMap;
import java.util.Map;

import okhttp3.OkHttpClient;

public class WordCounterTask extends Task<Integer> {

    public WordCounterTask(OkHttpClient client) {
        super(client);
    }

    @Override
    protected Integer processContent(String content) {
        String[] words = content.split("\\s+");
        Map<String, Integer> occurrences = new HashMap<>();
        for (String word : words) {
            word = word.toLowerCase();
            Integer oldCount = occurrences.get(word);
            if (oldCount == null) {
                oldCount = 0;
            }
            occurrences.put(word, oldCount + 1);
        }
        Integer count = occurrences.get(Constants.KEY.toLowerCase());
        return (count == null) ? 0 : count;
    }
}

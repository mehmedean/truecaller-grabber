package com.mbkopmaz.truecallergrabber.task;

import com.mbkopmaz.truecallergrabber.util.Constants;

import okhttp3.OkHttpClient;

public class TheIndexthCharacterTask extends Task<Character> {

    public TheIndexthCharacterTask(OkHttpClient client) {
        super(client);
    }

    @Override
    protected Character processContent(String content) {
        Character theIndexthCharacter = null;
        if (content.length() >= Constants.INDEX) {
            theIndexthCharacter = content.charAt(Constants.INDEX);
        } else {
            errorCode = Constants.ERROR_CODE_CONTENT_TOO_SHORT;
        }
        return theIndexthCharacter;
    }
}

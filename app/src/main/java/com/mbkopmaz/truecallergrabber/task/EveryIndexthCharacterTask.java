package com.mbkopmaz.truecallergrabber.task;

import com.mbkopmaz.truecallergrabber.util.Constants;

import okhttp3.OkHttpClient;

public class EveryIndexthCharacterTask extends Task<Character[]> {

    public EveryIndexthCharacterTask(OkHttpClient client) {
        super(client);
    }

    @Override
    protected Character[] processContent(String content) {
        Character[] every10thCharacter = null;
        if (content.length() >= Constants.INDEX) {
            every10thCharacter = new Character[content.length() / Constants.INDEX];
            for (int i = Constants.INDEX - 1, index = 0; i < content.length(); i += Constants.INDEX, index++) {
                every10thCharacter[index] = content.charAt(i);
            }
        } else {
            errorCode = Constants.ERROR_CODE_CONTENT_TOO_SHORT;
        }
        return every10thCharacter;
    }
}

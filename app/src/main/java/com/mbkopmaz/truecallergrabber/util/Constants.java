package com.mbkopmaz.truecallergrabber.util;

public class Constants {
    public static final int INDEX = 10;
    public static final String KEY = "truecaller";
    public static final String URL = "http://www.truecaller.com/support";

    public static final int ERROR_CODE_CONTENT_TOO_SHORT = 1;
}

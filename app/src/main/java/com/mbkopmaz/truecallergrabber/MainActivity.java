package com.mbkopmaz.truecallergrabber;

import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mbkopmaz.truecallergrabber.task.TaskHandler;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements TaskHandler.UiListener {

    private TaskHandler taskHandler;

    @Bind(R.id.start_button)
    Button startButton;
    @Bind(R.id.clear_button)
    Button clearButton;
    @Bind(R.id.progress_bar)
    ProgressBar progressBar;
    @Bind(R.id.scroll_view)
    ScrollView scrollView;
    @Bind(R.id.task1_label)
    TextView task1LabelView;
    @Bind(R.id.task2_label)
    TextView task2LabelView;
    @Bind(R.id.task3_label)
    TextView task3LabelView;
    @Bind(R.id.task1_text)
    TextView task1TextView;
    @Bind(R.id.task2_text)
    TextView task2TextView;
    @Bind(R.id.task3_text)
    TextView task3TextView;
    @Bind(R.id.task1_button)
    Button task1AnchorButton;
    @Bind(R.id.task2_button)
    Button task2AnchorButton;
    @Bind(R.id.task3_button)
    Button task3AnchorButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        taskHandler = new TaskHandler(getApplicationContext());
        taskHandler.setUiListener(this);

        setOnClickListeners();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        taskHandler.setUiListener(null);
    }

    void setOnClickListeners() {
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetUI();
                startButton.setEnabled(false);
                setProgressVisibility(true);
                taskHandler.runTasks();
            }
        });

        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetUI();
            }
        });

        task1AnchorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scrollView.smoothScrollTo(0, task1LabelView.getTop());
            }
        });

        task2AnchorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scrollView.smoothScrollTo(0, task2LabelView.getTop());
            }
        });

        task3AnchorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scrollView.smoothScrollTo(0, task3LabelView.getTop());
            }
        });
    }

    void resetUI() {
        task1TextView.setText("");
        task2TextView.setText("");
        task3TextView.setText("");
        startButton.setEnabled(true);
        clearButton.setEnabled(false);
        task1AnchorButton.setEnabled(false);
        task2AnchorButton.setEnabled(false);
        task3AnchorButton.setEnabled(false);
        setProgressVisibility(false);
    }

    public void setProgressVisibility(Boolean visibility) {
        progressBar.setVisibility(visibility ? View.VISIBLE : View.GONE);
    }

    @Override
    public void appendTask2Text(String text) {
        task2TextView.append(text);
    }

    @Override
    public void onTask1Finished(String text) {
        task1TextView.setText(text);
        task1AnchorButton.setEnabled(true);
    }

    @Override
    public void onTask2Finished() {
        task2AnchorButton.setEnabled(true);
    }

    @Override
    public void onTask3Finished(String text) {
        task3TextView.setText(text);
        task3AnchorButton.setEnabled(true);
    }

    @Override
    public void onTasksFinished() {
        setProgressVisibility(false);
        startButton.setEnabled(true);
        clearButton.setEnabled(true);
    }

    @Override
    public void showAlertDialog(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Exception")
                .setMessage(text)
                .setCancelable(false)
                .setPositiveButton("OK", null)
                .show();
        resetUI();
    }
}

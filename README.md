# TRUECALLER GRABBER #

Truecaller Grabber is a small application that retrieves the source file of Truecaller support page and then grabs the 10th (Task 1) and every 10th character (Task 2) and finds how many times the word 'truecaller' case insensitively appears in the text as a standalone word (Task 3). Main points about the application are mentioned below.

## 1. Libraries ##
- Square’s OkHttp library is used as the HTTP client. It automatically covers many problems encountered while doing requests. All tasks uses same instance to make asynchronous requests.
- Square’s Retrofit library is not used since it seems unnecessary.
- Square’s Otto library is not used as the event bus in order to use callback mechanisms to prove developer’s knowledge about them.
- Jake Wharton’s Butterknife library is used for casting views in the layout. It crashes the app while opening if a view is not found, therefore, a working application indicates non-null views and direct use of views without any null checks.

## 2. Architecture ##
- Main components of the application are as follows:
    + MainActivity: Does all UI related work and sets listeners to buttons, implements the “UiListener” interface of “TaskHandler” instance it has.
    + TaskHandler: Creates and runs tasks, then notifies “MainActivity” for related UI updates via “UiListener” interface. Implements the “Listener” interface in “Task” class.
    + Task: The abstract class that task classes extend.
    + TheIndexthCharacterTask: An extension of Task class, implements processContent() which returns the 10th character or null if not found.
    + EveryIndexthCharacterTask: An extension of Task class, implements “processContent()” which returns every 10th character as Character array or null if not found.
    + WordCounterTask: An extension of Task class, implements “processContent()” which returns the number of occurrences of the word “truecaller”.
    + Constants: Includes the constants used in the application. Truecaller support page URL, index and search keyword are stored here.
- There is only one layout, which is for “MainActivity”, and it has following components:
    + Introduction text at the top.
    + Start button that starts tasks simultaneously when clicked.
    + Progress bar on the right of Start button that shows up while tasks are running.
    + Three TextViews for each task result at the bottom of Start button that are updated when the related task finishes. Each has a label on top of them.
    + Clear button on the left of Start button which is activated when all tasks are finished. It resets the UI when clicked.
    + Three anchor buttons on the right of Start button that is activated when related task finishes. Each one jumps to the related result text below them.
    + Fragments are not used since there is no need.

## 3. Error Handling ##
- If a problem occurs while doing the request over the network (like HTTP 404), a dialog notifies the user with a generic error message and UI is reseted.
- If content is successfully read from the URL but there is a problem calculating the result (e.g. page content is smaller than 10 characters), related error message is shown on result view.

## 4. Testability ##
- An example set of test cases for “TaskHandler” can be found in “TaskHandlerTest”.

## 5. How to Use? ##
- Open the app, press the Start button, wait until all tasks are completed and then check the results.

## 6. Other ##
- Developed using Android Studio 2.1 on OS X El Capitan